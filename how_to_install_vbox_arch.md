# How to install vbox on arch system

on host

```sh
pacman -S virtualbox virtualbox-host-dkms virtualbox-guest-iso linux-headers
yay -S virtualbox-ext-oracle
```

on guest

```sh
pacman -S virtualbox-guest-utils linux-headers
```
