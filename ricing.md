# Ricing version 0.1

## Changing urxvt font
First, you have to create a file for configuration urxvt on `~/.Xresources`, add this:

```sh
URxvt.font: xft:Noto Sans mono:pixelsize=12
```

it will set Noto Sans Mono with size 12 pixel.

you have to load Xresources before launching X (in this case X with i3), then add this into `~/.xinitrc`

```sh
# load Xresources before X
[[ -f ~/.Xresources ]] && xrdb -merge -I$HOME ~/.Xresources

# start i3
exec i3
```
