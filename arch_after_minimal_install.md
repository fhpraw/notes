# After minimal install Arch/Arch Based

You should check internet connection, make sure it keeps online during installation. if not you have to install `networkmanager`, enable it automatically by invoking

```sh
systemctl enable NetworkManager.service
```

right after `chroot` during post installation.

## Adding User
adding general user `yourusername` for daily use, put it into wheel group
```sh
  useradd -m -g wheel yourusername
  passwd yourusername
```

## Edit /etc/sudoers
uncomment this part to grant wheel as sudoers
```sh
  %wheel ALL=(ALL) ALL
```

## Installing Graphical User Interface
we need:
  1. X.org (GUI foundation)
  2. i3-gaps (Window Manager)
  3. i3blocks (for displaying status)
  4. dmenu (simple launcher)
  5. urxvt (terminal)
  6. noto-fonts (universal font provide by google)

```sh
  pacman -S xorg-server xorg-xinit
  pacman -S i3-gaps
  pacman -S i3blocks
  pacman -S dmenu
  pacman -S rxvt-unicode
  pacman -S noto-fonts
```

## Make the X server start i3
In ~/.xinitrc, put:
```sh
  exec i3
```

then, you can start X (with i3 installed) by giving command:
```sh
  startx
```

This will read ~/.xinitrc to know what to start

## Set Resolution Using Xrandr
displaying all supported resolution
```sh
xrandr
```

it will look like this

```txt
Screen 0: minimum 8 x 8, current 1920 x 1080, maximum 32767 x 32767
eDP1 connected primary 1920x1080+0+0 (normal left inverted right x axis y axis) 310mm x 170mm
   1920x1080     59.98*+  59.93
   1680x1050     59.95    59.88
   1400x1050     59.98
   1600x900      60.00    59.95    59.82
   1280x1024     60.02
   1400x900      59.96    59.88
   1280x960      60.00
   1368x768      60.00    59.88    59.85
   1280x800      59.81    59.91
   1280x720      59.86    60.00    59.74
   1024x768      60.00
   1024x576      60.00    59.90    59.82
   960x540       60.00    59.63    59.82
   800x600       60.32    56.25
   864x486       60.00    59.92    59.57
   640x480       59.94
   720x405       59.51    60.00    58.99
   640x360       59.84    59.32    60.00
DP1 disconnected (normal left inverted right x axis y axis)
DP2 disconnected (normal left inverted right x axis y axis)
HDMI1 disconnected (normal left inverted right x axis y axis)
HDMI2 disconnected (normal left inverted right x axis y axis)
VIRTUAL1 disconnected (normal left inverted right x axis y axis)

```

based on information above, the active monitor name is `eDP1`

you can change resolution (ex: 1280x800) on the fly by simply typing this command:

```sh
xrandr --output eDP1 --mode 1280x800
```

you can also generate settings for customized resolution (1920x1080), if you want 1920x1080, then
```sh
cvt 1920 1080
```

it will look like this

```txt
# 1920x1080 59.96 Hz (CVT 2.07M9) hsync: 67.16 kHz; pclk: 173.00 MHz
Modeline "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
```

then copy the second line into this command

```sh
xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
```

add that resolution mode into active monitor

```sh
xrandr --addmode eDP1 "1920x1080_60.00"
```

set it

```sh
xrandr --output eDP1 --mode 1920x1080_60.00
```

if you want to set permanently, add/edit `/etc/X11/xorg.conf.d/10-monitor.conf`

```txt
Section "Monitor"
  Identifier "eDP1"
  Modeline "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
  Option "PreferredMode" "1920x1080_60.00"
EndSection
```
## Installing yay
First install git

```sh
sudo pacman -S git
```

then

```sh
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
```

## make a proper urxvt terminal with gruvbox theme and google noto fonts
urxvt color theme and font altered by editing ~/.Xresources.
For setting font:

```txt
Urxvt.font: xft:Noto Sans Mono:pixelsize=14
```

to remove scrollbar:

```txt
Urxvt.scrollBar: false
```

reduce space between letter

```txt
Urxvt.letterSpace: -1
```

set gruvbox theme:
```txt
*background: #282828
*foreground: #ebdbb2
*color0:  #282828
*color8:  #928374
*color1:  #cc241d
*color9:  #fb4934
*color2:  #98971a
*color10: #b8bb26
*color3:  #d79921
*color11: #fabd2f
*color4:  #458588
*color12: #83a598
*color5:  #b16286
*color13: #d3869b
*color6:  #689d6a
*color14: #8ec07c
*color7:  #a89984
*color15: #ebdbb2
```
